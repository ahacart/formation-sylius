SELF_DIR := $(dir $(lastword $(MAKEFILE_LIST)))
include $(SELF_DIR)/.project-basics/Makefile.mk

DC=docker-compose -p{{sylius}} -f .project-basics/docker/docker-compose.yml -f .project-basics/docker/docker-compose-traefik.yml

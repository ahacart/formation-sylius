<?php

// src/Form/Extension/AddressTypeExtension.php

namespace App\Form\Extension;

use Sylius\Bundle\AddressingBundle\Form\Type\AddressType;
use Symfony\Component\Form\AbstractTypeExtension;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\FormBuilderInterface;

class AddressTypeExtension extends AbstractTypeExtension
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            // Adding new fields works just like in the parent form type.
            ->add('mobileNumber', TelType::class, [
                'required' => false,
                'label' => 'app.form.customer.mobile_number',
            ]);
    }

    public static function getExtendedTypes(): iterable
    {
        return [AddressType::class];
    }
}

<?php
namespace App\Repository;
use Sylius\Bundle\ResourceBundle\Doctrine\ORM\EntityRepository;

class SupplierRepository extends EntityRepository
{
    public function findForStore($id)
    {
        return $this->find($id);
    }
}

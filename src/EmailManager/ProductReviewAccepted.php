<?php

// src/EmailManager/ProductReviewAccepted.php

namespace App\EmailManager;

use Sylius\Component\Mailer\Sender\SenderInterface;
use Sylius\Component\Review\Model\ReviewInterface;

class ProductReviewAccepted
{
    private SenderInterface $sender;

    public function __construct(SenderInterface $sender)
    {
        $this->sender = $sender;
    }

    public function __invoke(ReviewInterface $review): void
    {
        if (!$author = $review->getAuthor()) {
            return;
        }

        if (!$email = $author->getEmail()) {
            return;
        }

        $this->sender->send('product_review_accepted', [$email], ['review' => $review]);
    }
}

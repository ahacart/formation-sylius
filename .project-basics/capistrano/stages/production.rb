server '0.0.0.0', user: 'user', roles: %w{app}

set :branch, 'master'

set :ssh_options, {
    port: 2121
}

SSHKit.config.command_map[:composer] = "php #{shared_path.join("composer.phar")}"
SSHKit.config.command_map[:fpm] = 'sudo /etc/init.d/php-fpm'
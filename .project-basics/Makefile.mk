DC=docker-compose

.DEFAULT_GOAL := help

.PHONY: help

help:  ##Display this help
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n\nTargets:\n"} /^[a-zA-Z_-]+:.*?##/ { printf "  \033[36m%-10s\033[0m %s\n", $$1, $$2 }' $(MAKEFILE_LIST)

up: ##Start Docker
	$(DC) up -d --build

do: ##Stop Docher
	$(DC) down

ex: ##Connect to PHP
	$(DC) exec --user=www-data php /bin/bash

exa: ##Connect to PHP Admin
	$(DC) exec --user=root php /bin/bash

ai: ##Install Assets with webpack-encore
	$(DC) run php yarn install

ab: ##Build Assets with webpack-encore
	$(DC) run php ./node_modules/.bin/encore production

cc: ##Clean cache
	$(DC) exec php php bin/console c:c


